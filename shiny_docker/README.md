### Build the docker

```
$ cd shiny_docker/
$ sudo docker build -t dicerna_dash .
```


### Run the docker

```
$ sudo docker run --rm -p 3838:3838 -v <local directory containing Scope repsitory>:/srv/shiny-server/:ro -v /srv/shinylog/:/var/log/shiny-server/:rw dicerna_dash
```





