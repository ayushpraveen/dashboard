files.sources <- list.files("./R", full.names = T)
sapply(files.sources, source)

##### Load the signature / reference information
mapping_id_table_path <- "data/mapping_table.csv"
biblography_table_path <- "data/ref_table.csv"

message("INFO: ", "Loading mapping table start")
mapping_table <- read.csv(mapping_id_table_path)
message("INFO: ", "Loading mapping table complete")
gene_list <- mapping_table$Gene
message("INFO: ", "Loading biblography table start")
bibl_table <- read.csv(biblography_table_path)
message("INFO: ", "Loading biblography table complete")

tissue_list <- c("adrenal", "appendix", "bone marrow", "brain", "colon", "duodenum", "endometrium", "esophagus", "fat",
"gall bladder", "heart", "kidney", "liver", "lung", "lymph node", "ovary", "pancreas", "placenta", "prostate", "salivary gland", 
"skin", "small intestine", "spleen", "stomach", "testis", "thyroid", "urinary bladder") 
#####
