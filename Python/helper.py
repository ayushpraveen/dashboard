import pandas as pd
import requests as req
import time
import csv
import sys
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time
from bs4 import BeautifulSoup
import pandas as pd
import requests
import urlopen
import urllib.request
import re
import subprocess
import os
import tempfile


def get_uniprot_info(mapping_table, selected_genes):
       
    file_dir = tempfile.mkdtemp()

    cwd = os.getcwd()
    uniprot_ids = mapping_table['uniprot_id'][mapping_table['Gene'].isin(selected_genes)].tolist()
    uniprot_ids = [x for x in uniprot_ids if str(x) != 'nan']

    os.chdir(file_dir)
    with open('uniprot_file.txt', 'w') as f:
        for uni_id in uniprot_ids:
            file_name = 'https://www.uniprot.org/uniprot/' + uni_id + '.txt'
            f.write("%s\n" % file_name)
    f.close()


    subprocess.call(["wget", "-i", "uniprot_file.txt"])
    os.chdir(cwd)
    
    substr = "CC" # Substring to search for.
    gene_info_dict = {}
    for gene in selected_genes:
        uniprot_id = mapping_table['uniprot_id'][mapping_table['Gene'] == gene].tolist()[0]
        with open(file_dir+'/'+uniprot_id+'.txt', 'rt') as myfile:
            content = []
            for line in myfile:
                if line.find(substr) != -1:
                    ab = line.strip(substr)
                    content.append(ab)
                text_data = " ".join(content)
                text_data=text_data.split("-!-")
        for i in range(0, len(text_data)):
            if text_data[i].find("FUNCTION:") != -1:
                function = text_data[i]
            if text_data[i].find("SUBCELLULAR LOCATION:") != -1:
                subcellular_location = text_data[i]
            if text_data[i].find("TISSUE SPECIFICITY:") != -1:
                tissue = text_data[i]
        myfile.close()
        gene_info_dict[gene] = {}
        gene_info_dict[gene]['uniprot_id'] = uniprot_id
        gene_info_dict[gene]['function'] = function
        gene_info_dict[gene]['subcellular_location'] = subcellular_location
        gene_info_dict[gene]['tissue'] = tissue

    uniprot_df = pd.DataFrame.from_dict(gene_info_dict)

    return(uniprot_df)

def get_entrez_info(mapping_table, selected_genes):
       
    entrez_ids = mapping_table['entrez_id'][mapping_table['Gene'].isin(selected_genes)].tolist()
    entrez_ids = [x for x in entrez_ids if str(x) != 'nan']

    gene_info_dict = {}
    for gene in selected_genes:
        entrez_id = mapping_table['entrez_id'][mapping_table['Gene'] == gene].tolist()[0]
        url = 'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi?db=gene&id='+str(entrez_id)+'&rettype=text&retmode=text'
        time.sleep(0.5)
        r = req.get(url, allow_redirects=True)
        info = r.text
        info = info.split("\n")
        for x in info:
            if x.find("<Summary>") != -1:
                summ = x.split("<Summary>")[1].strip("</Summary>")
            if x.find("<Description>") != -1:
                desc = x.split("<Description>")[1].strip("</Description>")
            if x.find("<OtherAliases>") != -1:
                aliases = x.split("<OtherAliases>")[1].strip("</OtherAliases>")
                        
        gene_info_dict[gene] = {}
        gene_info_dict[gene]['entrez_id'] = entrez_id
        gene_info_dict[gene]['aliases'] = aliases
        gene_info_dict[gene]['description'] = desc
        gene_info_dict[gene]['summary'] = summ
       
     
    entrez_df = pd.DataFrame.from_dict(gene_info_dict)
    entrez_df.head()

    return(entrez_df)

def get_omim_info(mapping_table, selected_genes):

    options = Options()
    options.headless = True

    base_url = "https://omim.org/entry/"

    gene_info_dict = {}
    for gene in selected_genes:
        omim_id = mapping_table['omim_id'][mapping_table['Gene'] == gene].tolist()[0]
        website_url = base_url + str(omim_id)
        driver = webdriver.Chrome(executable_path="/srv/shiny-server/chromedriver", chrome_options=options)
        driver.get(website_url)
        content = driver.page_source
        soup = BeautifulSoup(content)
        desc = []
        b = soup.find('div', attrs = {'id':'descriptionFold'})
        if b != None:
            p = b.find('p')
            des = p.text
            desc.append(des)
        desc = "/n ".join(desc)
        clone = []
        b = soup.find('div', attrs = {'id':'cloningFold'})
        if b!= None:
            p = b.find('p')
            clo = p.text
            clone.append(clo)
        clone = "/n ".join(clone)
        structure = []
        b = soup.find('div', attrs = {'id':'geneStructureFold'})
        if b!= None:
            p = b.find('p')
            struc = p.text
            structure.append(struc)
        structure = "/n ".join(structure)
        mapping = []
        b = soup.find('div', attrs = {'id':'mappingFold'})
        if b!= None:
            p = b.find('p')
            map_ = p.text
            mapping.append(map_)
        mapping = "/n ".join(mapping)
        mol = []
        b = soup.find('div', attrs = {'id':'molecularGeneticsFold'})
        if b!= None:
            p = b.find('p')
            mo = p.text
            mol.append(mo)
        mol = "/n ".join(mol)
        an_mo = []
        b = soup.find('div', attrs = {'id':'animalModelFold'})
        if b!= None:
            p = b.find('p')
            mod = p.text
            an_mo.append(mod)
        an_mo = "/n ".join(an_mo)
        function = []
        ab = soup.find('div', attrs={'id':'geneFunctionFold'})
        if ab != None:
            for p in ab.find_all('p'):
                func = p.text
                function.append(func)
        function = "/n ".join(function)
        driver.quit()
        
        gene_info_dict[gene] = {}
        gene_info_dict[gene]['omim_id'] = omim_id
        gene_info_dict[gene]['description'] = desc
        gene_info_dict[gene]['cloning'] = clone
        gene_info_dict[gene]['structure'] = structure
        gene_info_dict[gene]['mapping'] = mapping
        gene_info_dict[gene]['molecular_genetics'] = mol
        gene_info_dict[gene]['animal_model'] = an_mo
        gene_info_dict[gene]['function'] = function

    entrez_df = pd.DataFrame.from_dict(gene_info_dict)

    return(entrez_df)

