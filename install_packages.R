options(repos="http://cran.rstudio.com")

install.packages(c('devtools', 'Rcpp', 'roxygen2'))

install.packages('BiocManager')

BiocManager::install(c("RColorBrewer","RandomFields","RNetCDF","classInt","deldir","gstat","hdf5r","lidR","mapdata","maptools","mapview","ncdf4","proj4","raster","rgdal","rgeos","rlas","sf","sp","spacetime","spatstat","spdep","geoR","geosphere"))

BiocManager::install(c('rhdf5'))

BiocManager::install(c("shiny", "shinydashboard", "shinythemes", "shinyFiles", "shinymaterial", "ggplot2", "plotly"))
BiocManager::install(c("DT","reticulate","future","future.apply","gridExtra", "Matrix", "shinycssloaders", "qlcMatrix", "shinyjs", "shinyWidgets", "colourvalues")) # scope

devtools::install_github("nik01010/dashboardthemes")
devtools::install_github("r-spatial/leafgl")

