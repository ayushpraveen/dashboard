make_reactive_values <- function(){
  reactive_values <- reactiveValues()
  return(reactive_values)
}

initialize_reactive_values <- function(reactive_data_list){  #this needs to be separate for the reactive values to work. Otherwise it deletes a reactive values object and shiny gets confused
  reactive_data_list$dim_red_selection = NULL
  reactive_data_list$hdf5_file_path = NULL
  reactive_data_list$file_format = NULL
  reactive_data_list$file_contains_scvi = FALSE
  reactive_data_list$dim_red_options = NULL
  reactive_data_list$dim_red_matrix = vector(mode = "list", length = 2)
  reactive_data_list$pca_dim_x = vector(mode = "list", length = 2)
  reactive_data_list$pca_dim_y = vector(mode = "list", length = 2)
  reactive_data_list$gene_chosen = vector(mode = "list", length = 2)
  reactive_data_list$sig_experiment_names = vector(mode="list", length = 3) # Cesium experiments
  reactive_data_list$sig_names = vector(mode="list", length = 3) # Cesium signatures
  reactive_data_list$gene_exp_vec = vector(mode="list", length = 2)
  reactive_data_list$type_of_plot = vector(mode = "list", length = 3)
  reactive_data_list$label_values = vector(mode = "list", length = 2)
  reactive_data_list$gene_names_p1 = NULL
  reactive_data_list$gene_names_p2 = NULL
  reactive_data_list$gene_names_scatter = NULL
  reactive_data_list$metadata_label_type = vector(mode = "list", length = 2)
  reactive_data_list$tsne_plot1 = NULL #leaving this as two (Rather than list). This way when you update one, the other one doesn't auto update. In a list format, both tsne plots will update automatically causing delay.
  reactive_data_list$tsne_plot2 = NULL
  reactive_data_list$gene_list_input = vector(mode = "list", length = 2)
  reactive_data_list$gene_list = vector(mode = "list", length = 2)
  reactive_data_list$violin_label_plot = NULL
  reactive_data_list$metadata_label = vector(mode = "list", length = 2)
  reactive_data_list$metadata_labels = NULL
  reactive_data_list$all_metadata_label_information = NULL
  reactive_data_list$gene_gene_scatter_plot = NULL
  reactive_data_list$signature_gene_plot = NULL
  reactive_data_list$clust_gene_plot = NULL
  reactive_data_list$filter1_cell_labels = NULL
  reactive_data_list$total_cells = NULL
  reactive_data_list$n_genes = NULL
  reactive_data_list$clust_plot_list = list()
  reactive_data_list$sparse_matrix_format = NULL
  reactive_data_list$selected_sparse_matrix_format = vector(mode = "list", length = 2)
  reactive_data_list$hdf5_attributes_df = NULL
  reactive_data_list$mergedf_object = list(merge_df = NULL, y_axis_label = NULL)
  reactive_data_list$obsm_list = NULL
  reactive_data_list$expression_matrices = list("sparse_X" = NULL, "sparse_scVI" = NULL, "non_sparse_gene_exp_mat" = NULL)
  reactive_data_list$sparse_indptr_data = list("X" = list("i" = NULL, "p" = NULL, "x" = NULL), "scVI" =  list("i" = NULL, "p" = NULL, "x" = NULL))
  reactive_data_list$csc_mapping_table = list("X" = NULL, "scVI" = NULL)
  reactive_data_list$species = NULL
  
  return(reactive_data_list)
  
}
